const gameOver = document.querySelector("#gameover");
const winner = document.querySelector("#bravo");
const button = document.querySelector("#retry")
/* Compte les cellules traversées*/
let cellCounter = 0;
/* Compte les cellules de la table*/
let winCounter = 0;

/**
 * Créé un string pour une couleur RGB aléatoire
 * @returns 
 */
function randomRgbColor() {
    let r = Math.floor(Math.random()*(256));
    let g = Math.floor(Math.random()*(256));
    let b = Math.floor(Math.random()*(256));
    return "rgb(" + r + ", "+ g + ", " + b + ")";
}
/*
* Logique du jeu appelée à chaque hover au dessus d'une cellule
*/
let gameCheck = function (event) {
    const currentCell = event.currentTarget;
    let randomColor = randomRgbColor();

    // SI le background est blanc, on le colorie aléatoirement
    if (currentCell.style.backgroundColor == "white") {
        currentCell.style.backgroundColor = randomColor;
        cellCounter += 1;
        // SI toutes les cases sont coloriées, win
        if(cellCounter == winCounter) {
            winner.style.display = "block";
            button.style.display = "block";
            disableGame();
        }
    // Si le background est colorié, c'est perdu
    } else {
        gameOver.style.display = "block";
        button.style.display = "block";
        disableGame();
    }
};

// Enelève tous les listeners sur chaque cellule
let disableGame = function () {
    let table = document.querySelector('#table');
    let rowCount = table.rows.length;
    let colCount = table.rows[0].cells.length;

    for (let r = 0; r < rowCount; r++) {

        for (let c = 0; c < colCount; c++) {
            const cell = table.rows[r].cells[c];

            cell.removeEventListener('mouseover', gameCheck);

        }
    }

}

/**
 * Fonction permettant de créer un plateau de jeu avec une taille donnée.
 * 
 * @param size Le nombre de lignes (ou colonnes) du plateau de jeu.
 * @param cellSize La taille des cellules en pixels (doit être un nombre)
 */
function createTable(size, cellSize) {
    let table = document.createElement('table');
    table.id = "table";
    for (let i = 0; i < size; ++i) {
        const row = table.insertRow();
        for (let j = 0; j < size; ++j) {
            let cell = row.insertCell();
            cell.id = `(${i}, ${j})`; // Approche ES6 de construire une chaîne de caractères avec des variables.
            cell.style.height = `${cellSize}px`;
            cell.style.width = `${cellSize}px`;

            cell.style.borderStyle = "solid";
            cell.style.borderWidth = "1px";
            cell.style.borderColor = "rgb(120, 120, 120)";
            if (i === 0 && j === 0) {
                cell.style.borderTopLeftRadius = "5px";
            }
            else if (i === 0 && j === size - 1) {
                cell.style.borderTopRightRadius = "5px";
            }
            else if (i === size - 1 && j === 0) {
                cell.style.borderBottomLeftRadius = "5px";
            }
            else if (i === size - 1 && j === size - 1) {
                cell.style.borderBottomRightRadius = "5px";
            }
            
            if(Math.random() < 0.95){
                cell.style.backgroundColor = "white";
                winCounter += 1;
            } else {
                cell.style.backgroundColor = "black";
            }
            
            cell.addEventListener('mouseover', gameCheck);
            
        }
    }

    table.style.backgroundColor = "white";
    table.style.borderSpacing = "0px";
    table.style.borderRadius = "5px";
    return table;
};


let contentElement = document.querySelector("#subcontent");
contentElement.appendChild(createTable(9, 50));

const retryButton = document.querySelector("#retry");
retryButton.addEventListener('click', event => {
    window.location = window.location;
})